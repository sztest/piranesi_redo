-- LUALOCALS < ---------------------------------------------------------
local dofile, pairs, string, table
    = dofile, pairs, string, table
local string_format, table_concat, table_sort
    = string.format, table.concat, table.sort
-- LUALOCALS > ---------------------------------------------------------

local version = dofile("./mods/piredo_api/version.lua")

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

local screenshots = {}
for i = 1, 6 do screenshots[i] = readbinary(".cdb-screen" .. i .. ".webp") end

local weblate = "https://hosted.weblate.org/projects/minetest/game-piranesi-redo/"
local transtext = {}
local translated = dofile("./mods/piredo_api/translated.lua")
local langs = dofile("./.cdb-langs.lua")
for k, v in pairs(translated) do
	if k ~= "en" then
		local n = langs[k]
		n = n and (n.l or n.en) or k
		transtext[#transtext + 1] = v == translated.en
		and string_format("[%s](%s%s/)✔", n, weblate, k)
		or string_format("[%s](%s%s/)(%d%%)",
			n, weblate, k, v / translated.en * 100)
	end
end
table_sort(transtext)
transtext = "\n\n### Translations\n\n" .. table_concat(transtext, " — ")
.. "\n\n… and more on [Weblate](" .. weblate .. ")"

return {
	user = "Warr1024",
	pkg = "piranesi_redo",
	dev_state = "ACTIVELY_DEVELOPED",
	version = version,
	type = "game",
	title = "Piranesi Restoration Project",
	short_description = "Discover the secrets of the space-bending house, in this polished"
	.. " remake of iarbat's award-winning original game.",
	tags = {"jam_game_2022", "puzzle", "singleplayer", "oneofakind__original"},
	content_warnings = {},
	license = "MIT",
	media_license = "CC-BY-SA-4.0",
	long_description = readtext("README.md") .. transtext,
	repo = "https://gitlab.com/sztest/piranesi_redo",
	issue_tracker = "https://gitlab.com/sztest/piranesi_redo/-/issues",
	maintainers = {"Warr1024"},
	screenshots = screenshots
}

-- luacheck: pop
