### *Explore the space-bending house and learn its secrets.*

A challenging, Myst-like adventure/puzzle game set in a unique and mysterious mansion.  Read the clues, find key items, solve puzzles, unlock new areas, learn the story of the labyrinth, and ultimately find your escape.

---

This is a polished and well-maintained recreation and continuation of the 2022 Game Jam silver medalist game, **Piranesi** by **[iarbat](https://content.minetest.net/users/cool_beans/)**.  Created using [Minetest Definition Ripper](https://content.minetest.net/packages/Warr1024/defripper/), and new original code.

## Features

- Complete game end to end, without the major bugs that plagued the original.
- Clean and simplified UI, dynamically expanding hotbar inventory, on-screen compass.
- Improved visual and sound effects, music.
- Improve accessibility, HUD tips for colorblind or low vision players.
