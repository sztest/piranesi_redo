#!/bin/sh
set -ex

conv() {
	IN="$1"
	OUT="$2"
	convert ../../.cdb-screen"$IN".jpg \
		-resize 576x360 \
		background-overlay.png \
		-composite .tmp.png
	pngquant --speed 1 --quality 0-100 --strip .tmp.png
	rm .tmp.png
	mv .tmp-fs8.png ../background"$OUT".png
}

conv 1
conv 2 .1
conv 3 .2
conv 4 .3
conv 5 .4
conv 6 .5