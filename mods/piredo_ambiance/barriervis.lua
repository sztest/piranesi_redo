-- LUALOCALS < ---------------------------------------------------------
local minetest, piredo_player, vector
    = minetest, piredo_player, vector
-- LUALOCALS > ---------------------------------------------------------

-- PARAM2 values:
-- 0 = show only when nearby (normal barriers)
-- 1 = show always ("glass floor" in final room)
-- 2 = show never (candle blockers around piano)

minetest.register_abm({
		nodenames = {"piredo_terrain:piranesi__barrier"},
		interval = 1,
		chance = 1,
		action = function(pos, node)
			if node.param2 == 2 then return end
			if node.param2 ~= 1 then
				local mp = piredo_player.mainplayer()
				local ppos = mp and mp:get_pos()
				if not ppos
				or pos.x < ppos.x - 1.5
				or pos.x > ppos.x + 1.5
				or pos.z < ppos.z - 1.5
				or pos.z > ppos.z + 1.5
				or pos.y < ppos.y - 1.5
				or pos.y > ppos.y + 3.5
				then return end
			end
			return minetest.add_particlespawner({
					amount = 10,
					time = 2,
					minpos = vector.offset(pos, -0.5, -0.5, -0.5),
					maxpos = vector.offset(pos, 0.5, 0.5, 0.5),
					minexptime = 1,
					maxexptime = 2,
					minsize = 0,
					maxsize = 0.35,
					texture = "piredo_ambiance_barrier.png"
				})
		end
	})
