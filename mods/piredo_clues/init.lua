-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, piredo_api, piredo_player, string
    = ipairs, minetest, pairs, piredo_api, piredo_player, string
local string_gsub, string_match
    = string.gsub, string.match
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local fse = minetest.formspec_escape
local S = piredo_api.translate

local function register_clue(basename, desc, subdesc, form_param, newtiles)
	local fullname = "piredo_terrain:piranesi__" .. basename
	piredo_api.add_groups(fullname, "clue", "collectable")
	local olddef = minetest.registered_items[fullname]
	local fulldesc = subdesc and S(desc .. " - " .. subdesc)
	minetest.override_item(fullname, {
			description = S(desc),
			tiles = newtiles or olddef.tiles,
			inventory_image = newtiles and newtiles[1] or olddef.tiles[1],
			wield_image = newtiles and newtiles[1] or olddef.tiles[1],
			wield_scale = {x = 1, y = 1, z = olddef.drawtype == "signlike" and 1 or 2},
			[modname .. "_fulldesc"] = fulldesc,
			on_use = function(stack, player)
				if not player then return end
				piredo_player.discover("read:" .. fullname)
				minetest.sound_play("piredo_clues_paper",
					{pos = player:get_pos()}, true)
				local info = minetest.get_player_information(player:get_player_name())
				local form = form_param(info.lang_code)
				minetest.show_formspec(player:get_player_name(), modname, form)
				if fulldesc then
					stack:get_meta():set_string("description", fulldesc)
					return stack
				end
			end,
			pointable = true
		})
end

local function formstart(w, h)
	return "size[" .. w .. "," .. h .. "]"
	.. "style_type[image_button;border=false;bgcolor=#00000000]"
	.. "image_button_exit[0,0;" .. w .. "," .. h
	.. ";" .. fse("[combine:1x1") .. ";close;]"
end

register_clue("star_chart", "Star Chart", nil, function()
		return formstart(20, 10) .. "image[0,0;25,12.5;piredo_clues_star_chart.png]"
	end)

local knownimgs = {}
for _, v in ipairs(minetest.get_dir_list(modpath .. "/textures", false)) do
	knownimgs[v] = true
end

local function find_translated(basename, lang)
	local exact = "piredo_clues_render_" .. lang .. "_" .. basename .. ".png"
	if knownimgs[exact] then return exact end
	if lang == "en" then return "[combine:1x1" end
	if not string_match(lang, "_") then return find_translated(basename, "en") end
	local parent = string_gsub(lang, "_.*", "")
	local pfound = find_translated(basename, parent)
	if pfound then return pfound end
	for k in pairs(knownimgs) do
		if string_match(k, "piredo_clues_render_" .. parent
			.. "_%w+_" .. basename .. "%.png") then
			return k
		end
	end
end

register_clue("clue_manu", "Manuscript", nil, function(lang)
		local img = find_translated("manuscript_0", lang)
		img = "[combine:1024x772:0,0=piredo_clues_back_manuscript.png:" ..
		"8,144=" .. img .. "^[mask:piredo_clues_back_manuscript_mask.png"
		return formstart(21, 15) .. "image[0,0;26.2,18.1125;" .. img .. "]"
	end)

local function register_clue_paper(basename, subdesc, imgbase, imgex)
	local ratio = 682/512
	local w = 20
	local h = ratio * w
	local size = (w * 1.25) .. "," .. (h * 1.25)
	local imagefunc = function(lang)
		local img = find_translated(imgbase .. "_0", lang)
		img = "[combine:512x682:0,0=piredo_clues_back_paper.png:40,40=" .. img
		if imgex then img = img .. imgex end
		local form = formstart(w, h) .. "image[0,0;" .. size .. ";" .. fse(img) .. "]"
		return form
	end
	return register_clue(basename, "Paper", subdesc, imagefunc,
		{"piredo_clues_inv_" .. imgbase .. ".png"})
end

register_clue_paper("clue_1", "Direction", "starting",
":40,450=piredo_clues_starting_diagram.png")
register_clue_paper("clue_2", "Machine", "machine")
register_clue_paper("clue_3", "Clock", "clock")
register_clue_paper("clue_4", "Furnaces", "forge")

local function register_clue_book(basename, subdesc, imgbase, color)
	local ratio = 682/512
	local w = 10
	local h = ratio * w
	local size = (w * 1.25) .. "," .. (h * 1.25)

	local imagefunc = function(lang)
		local img1 = find_translated(imgbase .. "_0", lang)
		local img2 = find_translated(imgbase .. "_1", lang)
		local back
		back = "[combine:512x682:0,0=piredo_clues_back_book.png"
		img1 = back .. ":32,32=" .. img1
		img2 = back .. ":32,32=" .. img2

		local form = formstart(w * 2, h)
		.. "image[0,0;" .. size .. ";" .. fse(img1) .. "]"
		.. "image[" .. w .. ",0;" .. size .. ";" .. fse(img2) .. "]"
		return form
	end

	return register_clue(basename, "Book", subdesc, imagefunc,
		{
			"piredo_clues_book_cover.png^[multiply:" .. color,
			"piredo_clues_book_cover.png^[multiply:" .. color,
			"piredo_clues_book_side_2.png^[multiply:" .. color
			.. "^piredo_clues_book_side_2_pages.png",
			"piredo_clues_book_side_1.png^[multiply:" .. color,
			"(piredo_clues_book_side_3.png^[multiply:" .. color
			.. "^piredo_clues_book_side_3_pages.png)^[transformFX",
			"piredo_clues_book_side_3.png^[multiply:" .. color
			.. "^piredo_clues_book_side_3_pages.png"
		})
end

register_clue_book("book", "Superstition", "study", "red")
register_clue_book("book1", "Folklore", "folklore", "blue")
register_clue_book("book2", "Cooking", "pot", "green")
register_clue_book("book3", "Poetry", "final", "yellow")
