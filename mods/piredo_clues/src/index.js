'use strict';

process.on('unhandledRejection', err => {
	throw err;
});

const { renderall } = require('./myrender');
const myopts = require('./myopts');
const mylangdb = require('./mylangdb');

const main = async () => await renderall(await mylangdb, await myopts);

main();
