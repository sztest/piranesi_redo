'use strict';

const fsp = require('fs')
	.promises;
const path = require('path');

module.exports = (async () => {
	const langdir = path.join(__dirname, 'languages');
	return Object.assign(...(await Promise.all((await fsp.readdir(langdir))
		.map(async ent => {
			const m = ent.match(/^([a-zA-Z0-9_]+)\.json$/);
			if(!m) return {};
			const raw = await fsp.readFile(path.join(langdir, ent));
			return {
				[m[1]]: JSON.parse(raw.toString())
			};
		}))));
})();