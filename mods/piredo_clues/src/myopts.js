'use strict';

const fonts = {
	book: 'Noto-Serif-Display-SemiBold',
	hand: 'Noto-Sans-Display-Medium-Italic',
	manu: 'Noto-Serif-Display-Bold-Italic',
};

const optpaper = {
	face: fonts.hand,
	points: 28,
	width: 512 - 80,
	height: 682 - 80,
	pages: 1
};
const optbook = {
	face: fonts.book,
	points: 28,
	width: 512 - 64,
	height: 682 - 64,
	pages: 2
};

module.exports = {
	clock: optpaper,
	final: Object.assign({}, optbook, {
		face: fonts.manu,
	}),
	folklore: optbook,
	forge: optpaper,
	machine: optpaper,
	manuscript: {
		face: fonts.manu,
		points: 48,
		width: 1024 - 16,
		height: 424,
		pages: 1
	},
	pot: optbook,
	starting: Object.assign({}, optpaper, {
		height: 380
	}),
	study: optbook
};
