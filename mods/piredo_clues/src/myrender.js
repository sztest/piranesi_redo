'use strict';

const fsp = require('fs')
	.promises;
const path = require('path');
const { tempfile, pngify, recompress } = require('./myutil');
const { fitpages } = require('./mysearch');
const { statecheck, statesave } = require('./mystatedb');

const outdir = path.join(__dirname, 'render');
const blank = fsp.readFile(path.join(__dirname, 'blank.png'));

const renderpage = async (outpng, pg, fit) => {
	const fitted = await fit();
	const text = fitted.pages[pg];
	if(text)
		await tempfile(async tmpf => {
			await pngify(text, fitted.opts, tmpf);
			await recompress(tmpf);
			await fsp.copyFile(tmpf, outpng);
		});
	else
		await fsp.writeFile(outpng, await blank);
};

const renderclue = async (name, langcode, text, opts) => {
	if(!text) return;
	let pending;
	const fit = async () => await (pending = pending || fitpages(text, opts));
	await Promise.all(range(opts.pages)
		.map(async pg => {
			const outname = `piredo_clues_render_${langcode}_${name}_${pg}.png`;
			const outpng = path.join(outdir, outname);
			if(await statecheck(outname, outpng, text, opts))
				return console.log({ outname, hit: true });
			await renderpage(outpng, pg, fit);
			await statesave(outname, outpng, text, opts);
			console.log({ outname, rendered: true });
		})
	);
};

const range = n => [...Array(n)
	.keys()
].sort();

const cluetext = (name, fallback, translated) => {
	const paras = [];
	for(const src of [fallback, translated])
		for(const [k, v] of Object.entries(src))
			if(k.startsWith(`clue:${name}`)){
				const parts = k.split(':');
				if(parts.length < 4) continue;
				paras[parts[2]] = paras[parts[2]] || [];
				const old = paras[parts[2]][parts[3]];
				if(!old || old.hash === parts[4])
					paras[parts[2]][parts[3]] = {
						text: v,
						hash: parts[4]
					};
			}
	return paras.map(p => p.map(x => x.text).join('  ')).join('\n\n');
};

const renderall = async (langdb, myopts) => {
	await Promise.all(Object.entries(langdb)
		.map(async ([langcode, textdb]) =>
			await Promise.all(Object.entries(myopts)
				.map(async ([name, opts]) => {
					const langtext = cluetext(name, langdb.en, textdb);
					const entext = cluetext(name, langdb.en, {});
					if(langtext.trim() && (langtext !== entext))
						await renderclue(name, langcode, langtext, opts)
				})
			)));
};

module.exports = { renderall };
