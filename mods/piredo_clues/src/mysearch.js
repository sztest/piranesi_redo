'use strict';

const { textsize } = require('./myutil');

const binsearch = async (min, max, attempt) => {
	if(await attempt(max))
		return max; // optimistic case
	max--;
	while(max > min) {
		const tryval = Math.ceil((min + max) / 2);
		if(await attempt(tryval, min, max))
			min = tryval;
		else
			max = tryval - 1;
	}
	return min;
};

const binsearchwrap = async (origmin, origmax, attempt) =>
	await binsearch(origmin, origmax, async (val, min, max) => {
		try {
			return await attempt(val, min, max);
		} catch (err) {
			err.searching = { val, min, max };
			if(err.status && err.status.signal === 'SIGINT')
				throw err;
			console.warn(err);
			return false;
		}
	});

const wrapline = async (text, opts) => {
	if(!/\S/.test(text)) return '';
	const words = text.split(/\s+/)
		.filter(x => x.length);
	const fit = await binsearchwrap(1, words.length, async val => {
		const size = await textsize(words.slice(0, val)
			.join(' '), opts);
		return size[0] <= opts.width;
	});
	if(fit >= words.length)
		return text;
	return words.slice(0, fit)
		.join(' ') + '\n' +
		await wrapline(words.slice(fit)
			.join(' '), opts);
};

const wrapall = async (text, opts) =>
	(await Promise.all(text.split('\n')
		.map(async x => await wrapline(x, opts))))
	.join('\n');

const paginate = async (text, opts) => {
	const lines = text.split('\n');
	const fit = await binsearchwrap(1, lines.length, async val => {
		const size = await textsize(lines.slice(0, val)
			.join('\n'), opts);
		return size[1] <= opts.height;
	});
	if(fit >= lines.length)
		return [text];
	return [
		lines.slice(0, fit)
		.join('\n'),
		...(await paginate(lines.slice(fit)
			.join('\n'), opts))
	];
};

const fitpages = async (text, opts) => {
	const scale = 4;
	let lastok;
	await binsearch(1, opts.points * scale, async val => {
		const myopts = Object.assign({}, opts, { points: val / scale });
		const wrapped = await wrapall(text, myopts);
		const pages = await paginate(wrapped, myopts);
		if(pages.length > opts.pages)
			return false;
		lastok = { opts: myopts, wrapped, pages };
		return true;
	});
	return lastok;
};

module.exports = {
	binsearch,
	binsearchwrap,
	wrapline,
	wrapall,
	paginate,
	fitpages
};
