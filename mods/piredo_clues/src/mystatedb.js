'use strict';

const fsp = require('fs')
	.promises;
const path = require('path');
const crypto = require('crypto');

const statepath = path.join(__dirname, 'statedb.json');
const loadstate = (async () => {
	try {
		return JSON.parse((await fsp.readFile(statepath))
			.toString());
	} catch (err) {
		if(err.code !== 'ENOENT')
			throw err;
		return {};
	}
})();

const hash = s => crypto.createHash('sha256')
	.update(s.toString())
	.digest('base64');

const statecheck = async (name, fullpath, text, opts) => {
	const statedb = await loadstate;

	const found = statedb[name];
	if(!found) return;

	let pnghash;
	try {
		pnghash = hash(await fsp.readFile(fullpath));
	} catch (err) {
		if(err.code !== 'ENOENT')
			throw err;
		return;
	}

	return found === hash(JSON.stringify([
		opts.face,
		opts.points,
		opts.width,
		opts.height,
		opts.pages,
		text,
		pnghash
	]));
};

const statesave = async (name, fullpath, text, opts) => {
	const statedb = await loadstate;
	statedb[name] = hash(JSON.stringify([
		opts.face,
		opts.points,
		opts.width,
		opts.height,
		opts.pages,
		text,
		hash(await fsp.readFile(fullpath))
	]));
	await fsp.writeFile(statepath, JSON.stringify(statedb, null, '\t'));
};

module.exports = { statecheck, statesave };
