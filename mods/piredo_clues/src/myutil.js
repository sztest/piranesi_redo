'use strict';

const fsp = require('fs')
	.promises;
const childproc = require('child_process');

const mysys = async (...args) => {
	const cmd = args.shift();
	const proc = childproc.spawn(cmd, args, {
		stdio: ['ignore', 'pipe', 'pipe']
	});
	const [out, err, status] = await Promise.all([
		new Promise((res, rej) => {
			proc.stdout.on('error', rej);
			const buffs = [];
			proc.stdout.on('data', x => buffs.push(x));
			proc.stdout.on('close', () => res(Buffer.concat(buffs)
				.toString()));
		}),
		new Promise((res, rej) => {
			proc.stderr.on('error', rej);
			const buffs = [];
			proc.stderr.on('data', x => buffs.push(x));
			proc.stderr.on('close', () => res(Buffer.concat(buffs)
				.toString()));
		}),
		new Promise((res, rej) => {
			proc.on('error', rej);
			proc.on('exit', (code, signal) => res({ code, signal }));
		}),
	]);
	if(!status.code && !status.signal)
		return out;
	throw Object.assign(Error(
		err ? err :
		status.signal ? `signal ${status.signal}` :
		`code ${status.code}`
	), { status });
};

const rmf = async fn => {
	try {
		await fsp.rm(fn);
	} catch (err) {
		if(err.code !== 'ENOENT')
			throw err;
	}
};

const pngify = async (text, opts, outpng) => {
	if(!/\S/.test(text))
		throw Error('blank text');
	await rmf(outpng);
	console.log({ font: opts.face, pointsize: opts.points, text: text.length });
	await mysys('convert', '-colorspace', 'gray', '-background', 'none', '-fill', 'black',
		'-font', opts.face, '-pointsize', opts.points, 'label:' + text,
		'-quality', 0, outpng);
};

const recompress = async (outpng) => {
	await mysys('optipng', outpng);
	await mysys('advpng', '-z', '-4', outpng);
};

let tmpid = new Date()
	.getTime() * 100;
const tempfile = async func => {
	const tmpng = `/tmp/cluerender-${++tmpid}.png`;
	try { return await func(tmpng); } finally { await rmf(tmpng); }
};

const textsize = async (text, opts) => tempfile(async outpng => {
	await pngify(text, opts, outpng);
	const txt = await mysys('identify', '-ping', '-format', '%w %h', outpng);
	return txt.split(' ');
});

module.exports = {
	mysys,
	rmf,
	pngify,
	recompress,
	tempfile,
	textsize
};
