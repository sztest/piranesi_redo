-- LUALOCALS < ---------------------------------------------------------
local minetest, piredo_api, vector
    = minetest, piredo_api, vector
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

function api.checkbarriers(pos, player)
	local start = player:get_pos()
	start.y = start.y + player:get_properties().eye_height
	local diff = vector.subtract(pos, start)
	local len = vector.length(diff)
	if len < 0.5 then return end
	local dir = vector.multiply(diff, 1 / len)
	local prev = start
	for i = 0, len, 0.1 do
		local p = vector.round(vector.add(start, vector.multiply(dir, i)))
		if not vector.equals(prev, p) then
			local node = minetest.get_node(p)
			local def = minetest.registered_nodes[node.name]
			if def and def.walkable and def.drawtype == "airlike"
			and not def.collision_box then
				return true
			end
		end
	end
end
