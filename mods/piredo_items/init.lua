-- LUALOCALS < ---------------------------------------------------------
local piredo_api
    = piredo_api
-- LUALOCALS > ---------------------------------------------------------

piredo_api.include("tradeitem")
piredo_api.include("collectables")
piredo_api.include("ratelimit")
piredo_api.include("effects")
piredo_api.include("checkbarriers")
piredo_api.include("tryinvoke")
piredo_api.include("placement")
piredo_api.include("itemuse")
piredo_api.include("hotfix")
