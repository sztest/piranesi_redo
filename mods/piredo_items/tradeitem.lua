-- LUALOCALS < ---------------------------------------------------------
local ItemStack, piredo_api
    = ItemStack, piredo_api
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

function api.trade_item(player, oldstack, newitem)
	oldstack = ItemStack(oldstack)
	newitem = ItemStack(newitem)
	if oldstack:get_count() == 1 then
		return function()
			return newitem
		end
	end
	local inv = player:get_inventory()
	if inv:room_for_item("main", newitem) then
		return function()
			inv:add_item("main", newitem)
			oldstack:take_item(1)
			return oldstack
		end
	end
end
