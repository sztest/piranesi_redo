-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, piredo_api, string
    = minetest, pairs, piredo_api, string
local string_format, string_sub
    = string.format, string.sub
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

-- on_apply_itemgroup -> either left or right click
-- on_whack_itemgroup -> left click
-- on_give_itemgroup -> right-click

function api.tryinvoke(pointed, node, stack, user, prefix)
	node = node or minetest.get_node_or_nil(pointed.under)
	local def = node and minetest.registered_nodes[node.name]
	for k, v in pairs(def or {}) do
		if piredo_api.starts_with(k, prefix) and minetest.get_item_group(
			stack:get_name(), string_sub(k, #prefix + 1)) > 0 then
			minetest.log("action", string_format(
					"player applies %q to %q at %s",
					k, node.name,
					minetest.pos_to_string(pointed.under)))
			return v(pointed.under, node, stack, user, pointed)
		end
	end
	return false
end
