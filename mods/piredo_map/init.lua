-- LUALOCALS < ---------------------------------------------------------
local piredo_api
    = piredo_api
-- LUALOCALS > ---------------------------------------------------------

piredo_api.include("statedb")
piredo_api.include("schematics")
piredo_api.include("triggers")
piredo_api.include("nodechanges")
piredo_api.include("getarea")
piredo_api.include("movement")
piredo_api.include("startup")
piredo_api.include("skybox")
piredo_api.include("cheat")
