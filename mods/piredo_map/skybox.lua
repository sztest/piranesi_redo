-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, piredo_api, piredo_player
    = ipairs, minetest, pairs, piredo_api, piredo_player
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

local skytextures = {}
for i = 1, 6 do skytextures[i] = "[combine:1x1^[noalpha" end
local skysettings = {
	[true] = {
		set_sky = {
			type = "regular",
			clouds = true,
			base_color = "#ffffff"
		},
		set_sun = {
			visible = true,
			sunrise_visible = true
		},
		set_moon = {
			visible = true
		},
		set_stars = {
			visible = true,
			count = 2500
		},
		set_lighting = {
			shadows = {intensity = 0.6}
		}
	},
	[false] = {
		set_sky = {
			type = "skybox",
			clouds = false,
			base_color = "#000000",
			textures = skytextures
		},
		set_sun = {
			visible = false,
			sunrise_visible = false
		},
		set_moon = {
			visible = false
		},
		set_stars = {
			visible = false
		},
		set_lighting = {
			shadows = {intensity = 0}
		}
	}
}
local setdaytime = false
local getdata = piredo_player.create_player_data()
minetest.register_globalstep(function()
		local daytime = api.statedb and api.statedb.daytime
		if daytime and daytime ~= setdaytime then
			minetest.set_timeofday(daytime)
			setdaytime = daytime
		end
		local sky = not not daytime
		for _, player in ipairs(minetest.get_connected_players()) do
			local data = getdata(player)
			if data.sky ~= sky then
				data.sky = sky
				for k, v in pairs(skysettings[sky]) do
					player[k](player, v)
				end
			end
		end
	end)
