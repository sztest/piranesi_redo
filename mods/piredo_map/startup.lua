-- LUALOCALS < ---------------------------------------------------------
local math, minetest, piredo_api, piredo_player, vector
    = math, minetest, piredo_api, piredo_player, vector
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

do
	local function startmap(startpos)
		api.putschem(vector.offset(startpos, -8, -1, -10), "outside")
		api.putschem(vector.offset(startpos, 2, -1, 30), "hall_z_plus")
		api.set_current_room(vector.offset(startpos, -8, -1, -10), "outside")
	end
	piredo_player.register_player_init(function(player)
			api.statedb.daytime = 0.35 + math_random() * 0.1
			api.savedb()
			local startpos = player:get_pos()
			player:set_pos(vector.offset(startpos, 0, 4, 0))
			startmap(startpos)
			minetest.after(0.5, startmap, startpos)
			player:set_look_horizontal(5.55189235059)
			player:set_look_vertical(-0.256563400043)
		end)
end
