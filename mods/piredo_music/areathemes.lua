-- LUALOCALS < ---------------------------------------------------------
local piredo_api, piredo_map, piredo_schems
    = piredo_api, piredo_map, piredo_schems
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

function api.areatheme(player)
	local area, sub = piredo_map.get_map_area(player)
	local key = "areatheme"
	if sub then key = key .. "_" .. sub end
	local schem = piredo_schems[area]
	return schem and schem[key]
end
