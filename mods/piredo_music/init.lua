-- LUALOCALS < ---------------------------------------------------------
local piredo_api, piredo_player
    = piredo_api, piredo_player
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

piredo_api.include("areathemes")
piredo_api.include("introsequence")
piredo_api.include("outrosequence")
piredo_api.include("loopdj")

piredo_player.register_playerstep(function(player, dtime)
		if piredo_player.is_endcredits() then
			return api.fade(1)
		end
		if api.introsequence(player, dtime) then return end
		if api.outrosequence(player, dtime) then return end
		return api.loopdj(api.areatheme(player))
	end)
