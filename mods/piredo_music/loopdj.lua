-- LUALOCALS < ---------------------------------------------------------
local minetest, os, piredo_api, tonumber
    = minetest, os, piredo_api, tonumber
local os_time
    = os.time
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

local offs = os_time() % 3600
minetest.register_globalstep(function(dtime)
		offs = (offs + dtime) % 3600
	end)

local faderate = 1/8

local soundid
local songname

local empty = {}
function api.loopdj(name, opts)
	opts = opts or empty
	if (not opts.force) and name == songname then return end
	if soundid then
		minetest.sound_fade(soundid, faderate, 0)
	end
	local volume = tonumber(minetest.settings:get("piranesi_music")) or 1
	if volume <= 0 then return end
	soundid = name and minetest.sound_play("piredo_music_" .. name, {
			start_time = opts.seek or offs,
			gain = 0.25 * volume,
			fade = faderate,
			loop = not opts.noloop,
		})
	songname = name
end

function api.fade(time)
	if not soundid then return end
	local volume = tonumber(minetest.settings:get("piranesi_music")) or 1
	if volume <= 0 then return end
	return minetest.sound_fade(soundid, 0.25 * volume / time, 0)
end
