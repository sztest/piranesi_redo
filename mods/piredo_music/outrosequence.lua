-- LUALOCALS < ---------------------------------------------------------
local minetest, piredo_api, piredo_map
    = minetest, piredo_api, piredo_map
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()
local modstore = minetest.get_mod_storage()

local storekey = "outro_sequence"
local function prevent_replay()
	modstore:set_string(storekey, "1")
end
local state = {
	done = (modstore:get_string(storekey) or "") ~= "" or nil
}

function api.outrosequence(_, dtime)
	if state.done then return end

	if state.silence then
		state.silence = state.silence + dtime
		if state.silence >= 12 then
			state.silence = nil
			state.done = true
		end

	elseif state.playing then
		state.playing = state.playing + dtime
		if state.playing >= 30 then
			api.fade(10)
			state.playing = nil
			state.silence = 0
		end

	elseif piredo_map.get_current_room() == "outside_ruined" then
		api.loopdj("first_realization",
			{seek = 10, force = true, noloop = true})
		state.playing = 0
		prevent_replay()
	else return end

	return true
end
