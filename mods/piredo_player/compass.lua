-- LUALOCALS < ---------------------------------------------------------
local minetest, piredo_api
    = minetest, piredo_api
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()
local modname = minetest.get_current_modname()

local getdata = api.create_player_data()

function api.hide_compass(player)
	local data = getdata(player)
	if data.hud then player:hud_remove(data.hud) end
end

api.register_on_playerswap(function(newp, oldp)
		if newp and not api.is_endcredits() then
			local ndata = getdata(newp)
			ndata.hud = ndata.hud or newp:hud_add({
					[piredo_api.hudtype] = "compass",
					size = {x = 96, y = 96},
					text = modname .. "_compass.png",
					position = {x = 0.5, y = 0.5},
					alignment = {x = 0, y = 0},
					offset = {x = 0, y = 0},
					direction = 1
				})
		end
		if oldp then api.hide_compass(oldp) end
	end)
