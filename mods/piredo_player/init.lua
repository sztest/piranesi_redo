-- LUALOCALS < ---------------------------------------------------------
local piredo_api
    = piredo_api
-- LUALOCALS > ---------------------------------------------------------

piredo_api.include("playerapi")
piredo_api.include("discover")
piredo_api.include("timer")
piredo_api.include("screenfade")
piredo_api.include("endcredits")
piredo_api.include("savepos")
piredo_api.include("playerinit")
piredo_api.include("compass")
piredo_api.include("colorhuds")
piredo_api.include("wieldtip")
piredo_api.include("model")
piredo_api.include("invadd")
piredo_api.include("hotbar")
