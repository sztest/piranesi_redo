-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, piredo_items, type, vector
    = minetest, pairs, piredo_items, type, vector
-- LUALOCALS > ---------------------------------------------------------

local function breaknode(pos, node)
	minetest.remove_node(pos)
	piredo_items.digeffect(pos, node)
	minetest.check_for_falling(vector.offset(pos, 0, 1, 0))
end

local function diggable_tiles(nodename, overlay)
	overlay = "^(piredo_diggable_" .. overlay .. ".png^[opacity:32)"
	local def = minetest.registered_nodes[nodename]
	local tiles = {}
	for k, v in pairs(def.tiles) do
		if type(v) == "table" then
			local nt = {}
			for k2, v2 in pairs(v) do nt[k2] = v2 end
			nt.name = nt.name .. overlay
			tiles[k] = nt
		else
			tiles[k] = v .. overlay
		end
	end
	minetest.override_item(nodename, {tiles = tiles})
end

minetest.override_item("piredo_terrain:piranesi__dirt_with_grass", {
		on_whack_shovel = breaknode
	})
diggable_tiles("piredo_terrain:piranesi__dirt_with_grass", "shovel")

minetest.override_item("piredo_terrain:default__dirt_with_coniferous_litter", {
		on_whack_shovel = function(pos, node)

			local an = minetest.get_node(vector.offset(pos, 0, 1, 0))
			local adef = minetest.registered_nodes[an.name]
			if not (adef and adef.air_equivalent) then
				minetest.spawn_falling_node(vector.offset(pos, 0, 1, 0))
			end

			return breaknode(pos, node) or nil
		end
	})
diggable_tiles("piredo_terrain:default__dirt_with_coniferous_litter", "shovel")

minetest.override_item("piredo_terrain:piranesi__pine_tree", {
		on_whack_axe = breaknode
	})
diggable_tiles("piredo_terrain:piranesi__pine_tree", "axe")

minetest.override_item("piredo_terrain:morelights_vintage__lantern_f", {
		groups = {falling_node = 1}
	})
