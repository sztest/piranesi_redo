-- LUALOCALS < ---------------------------------------------------------
local minetest, piredo_api
    = minetest, piredo_api
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

function api.magicsound(pos)
	return minetest.sound_play("xdecor_enchanting", {pos = pos}, true)
end

function api.magic(pos, sound)
	if sound then api.magicsound(pos) end
	return minetest.add_particlespawner({
			amount = 50,
			time = 1,
			pos = pos,
			radius = {min = 0, max = 0.5},
			acc = {x = 0, y = 3, z = 0},
			exptime = {min = 1, max = 3},
			size = {min = 0.5, max = 2},
			glow = 10,
			texture = "piranesi_magic_particle.png",
		})
end

piredo_api.include("doors")
piredo_api.include("digging")
piredo_api.include("gears")
piredo_api.include("flowerpots")
piredo_api.include("potions")
piredo_api.include("crowns")
piredo_api.include("candles")
piredo_api.include("clock")
piredo_api.include("finalaltar")
