-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, piredo_api, piredo_items, piredo_map,
      piredo_player, string, vector
    = minetest, pairs, piredo_api, piredo_items, piredo_map,
      piredo_player, string, vector
local string_sub
    = string.sub
-- LUALOCALS > ---------------------------------------------------------

local api = piredo_api.get_mod_api()

piredo_api.add_groups("piredo_terrain:piranesi__bottle_blue_block", "potion_b")
piredo_api.add_groups("piredo_terrain:piranesi__bottle_red_block", "potion_r")
piredo_api.add_groups("piredo_terrain:piranesi__bottle_yellow_block", "potion_y")
piredo_api.add_groups("piredo_terrain:piranesi__bottle_green_block", "potion_g")
piredo_api.add_groups("piredo_terrain:piranesi__bottle_purple", "potion_x")
piredo_api.add_groups("piredo_terrain:piranesi__bottle_black", "potion_x")
piredo_api.add_groups("piredo_terrain:farming__salt", "potion_x")
piredo_api.add_groups("piredo_terrain:farming__pepper_ground", "potion_x")
piredo_api.add_groups("piredo_terrain:farming__rose_water", "potion_x")
piredo_api.add_groups("piredo_terrain:farming__bottle_ethanol", "potion_x")
piredo_api.add_groups("piredo_terrain:farming__soy_sauce", "potion_x")
piredo_api.add_groups("piredo_terrain:farming__vanilla_extract", "potion_x")

local modstore = minetest.get_mod_storage()

local formulae = {
	rybgyrb = "black",
	gbry = "purple"
}

for ingred in pairs({r = 1, g = 1, b = 1, y = 1, x = 1, n = 1}) do
	minetest.override_item("piredo_terrain:piranesi__cauldron", {
			["on_apply_potion_" .. ingred] = function(pos, node)
				api.magic(vector.offset(pos, 0, 0.5, 0))
				minetest.sound_play("default_water_footstep",
					{pos = pos, gain = 0.25, pitch = 3}, true)

				local meta = minetest.get_meta(pos)
				local recipe = meta:get_string("recipe") or ""
				recipe = ingred .. string_sub(recipe, 1, 9)
				meta:set_string("recipe", recipe)
				piredo_map.savechange(pos, node, "cauldron meta")

				for k, v in pairs(formulae) do
					if piredo_api.starts_with(recipe, k)
					and modstore:get_string("brewed_" .. k) == "" then
						piredo_player.discover("solve:brewed_" .. k)
						modstore:set_string("brewed_" .. k, "1")
						node.name = node.name .. "_" .. v
						minetest.set_node(pos, node)
						api.magicsound(pos)
					end
				end
			end
		})
end

piredo_api.add_groups("piredo_terrain:piranesi__bottle_purple", "potion_purple")
piredo_api.add_groups("piredo_terrain:piranesi__bottle_black", "potion_black")

piredo_api.add_groups("piredo_terrain:piranesi__bottle_block", "potion_empty")

for color, prize in pairs({purple = "crownblock1", black = "coin_totem"}) do
	minetest.override_item("piredo_terrain:piranesi__cauldron_" .. color, {
			on_apply_potion_empty = function(pos, node, stack, player)
				local item = "piredo_terrain:piranesi__bottle_" .. color

				local commit = piredo_items.trade_item(player, stack, item)
				if not commit then return end

				node.name = "piredo_terrain:piranesi__cauldron"
				minetest.swap_node(pos, node)

				minetest.sound_play("default_water_footstep",
					{pos = pos, gain = 0.25, pitch = 3}, true)
				piredo_player.discover("solve:bottled_" .. color)
				piredo_items.pickupeffect(pos, {name = item})
				return commit()
			end
		})
	minetest.override_item("piredo_terrain:piranesi__bowl_" .. color .. "_pre", {
			["on_apply_potion_" .. color] = function(pos, node)
				minetest.sound_play("default_water_footstep",
					{pos = pos, gain = 0.25, pitch = 3}, true)
				api.magic(vector.offset(pos, 0, 0.5, 0), true)

				piredo_player.discover("solve:bowl_" .. color)
				node.name = "piredo_terrain:piranesi__bowl_" .. color
				minetest.swap_node(pos, node)
				minetest.set_node(vector.offset(pos, 0, 1, 0), {
						name = "piredo_terrain:piranesi__" .. prize,
						param2 = 1
					})
				return "piredo_terrain:piranesi__bottle_block"
			end
		})
end
