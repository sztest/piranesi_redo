-- LUALOCALS < ---------------------------------------------------------
local piredo_api
    = piredo_api
-- LUALOCALS > ---------------------------------------------------------

return {
	areatheme_upstairs = "prison_unlock",
	title = piredo_api.translate("The Prison"),
	legend = {
		["a"] = {name = "piredo_terrain:default__silver_sandstone_brick"},
		["b"] = {name = "piredo_terrain:default__cobble"},
		["c"] = {name = "piredo_terrain:default__mossycobble"},
		["d"] = {name = "piredo_terrain:default__stone"},
		["."] = {name = "piredo_terrain:ambient"},
		["e"] = {name = "piredo_terrain:castle_gates__jail_door_a"},
		["f"] = {param2 = 3, name = "piredo_terrain:quartz__pillar"},
		["g"] = {name = "piredo_terrain:quartz__pillar"},
		["h"] = {name = "piredo_terrain:default__lava_source"},
		["i"] = {param2 = 1, name = "piredo_terrain:castle_gates__jail_door_a"},
		["j"] = {param2 = 3, name = "piredo_terrain:castle_gates__jail_door_a"},
		["k"] = {name = "piredo_terrain:xdecor__stonepath"},
		["l"] = {param2 = 1, name = "piredo_terrain:stairs__stair_cobble"},
		["m"] = {param2 = 3, name = "piredo_terrain:stairs__stair_cobble"},
		["n"] = {param2 = 7, name = "piredo_terrain:default__lava_flowing"},
		["o"] = {param2 = 1, name = "piredo_terrain:piranesi__door_black"},
		["p"] = {param2 = 2, name = "piredo_terrain:ropes__ropeladder_bottom"},
		["q"] = {param2 = 3, name = "piredo_terrain:castle_farming__straw_dummy"},
		["r"] = {param2 = 2, name = "piredo_terrain:stairs__stair_cobble"},
		["s"] = {param2 = 1, name = "piredo_terrain:quartz__pillar"},
		["t"] = {name = "piredo_terrain:stairs__stair_cobble"},
		["u"] = {name = "piredo_terrain:ropes__rope_bottom"},
		["v"] = {param2 = 6, name = "piredo_terrain:default__lava_flowing"},
		["w"] = {param2 = 2, name = "piredo_terrain:quartz__pillar"},
		["x"] = {name = "piredo_terrain:doors__hidden"},
		["y"] = {param2 = 1, name = "piredo_terrain:doors__hidden"},
		["z"] = {param2 = 3, name = "piredo_terrain:doors__hidden"},
		["A"] = {param2 = 20, name = "piredo_terrain:morelights_vintage__smallblock"},
		["B"] = {param2 = 2, name = "piredo_terrain:ropes__ropeladder"},
		["C"] = {name = "piredo_terrain:ropes__rope"},
		["D"] = {param2 = 15, name = "piredo_terrain:default__lava_flowing"},
		["E"] = {param2 = 5, name = "piredo_terrain:ropes__wood1rope_block"},
		["F"] = {name = "piredo_terrain:quartz__chiseled"},
		["G"] = {name = "piredo_terrain:morelights__chain_ceiling_d"},
		["H"] = {name = "piredo_terrain:ropes__wood1rope_block"},
		["I"] = {param2 = 13, name = "piredo_terrain:default__lava_flowing"},
		["J"] = {param2 = 4, name = "piredo_terrain:xdecor__ivy"},
		["K"] = {name = "piredo_terrain:xdecor__cobweb"},
		["L"] = {param2 = 3, name = "piredo_terrain:xdecor__ivy"},
		["M"] = {param2 = 3, name = "piredo_terrain:default__bookshelf"},
		["N"] = {name = "piredo_terrain:morelights_vintage__lantern_f"},
		["O"] = {name = "piredo_terrain:piranesi__bowl_black_pre"},
		["P"] = {param2 = 5, name = "piredo_terrain:xdecor__ivy"},
		["Q"] = {param2 = 2, name = "piredo_terrain:default__bookshelf"},
		["R"] = {param2 = 8, name = "piredo_terrain:morelights_vintage__smallblock"},
		["S"] = {param2 = 16, name = "piredo_terrain:morelights_vintage__smallblock"},
		["T"] = {param2 = 2, name = "piredo_terrain:xdecor__ivy"},
		["U"] = {param2 = 2, name = "piredo_terrain:ropes__ropeladder_top"},
		["V"] = {name = "piredo_terrain:xdecor__ivy"},
		["W"] = {name = "piredo_terrain:morelights_modern__block"},
	},
	nodes = {
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abbbbbbbbbbbbbbcbbca",
			"abbdbccbbbbbbbccbbba",
			"abbbccccbcbbbbccbcca",
			"abcbbbbcbccbbbcbbcba",
			"abbbbbcbbccccbbbccba",
			"accbbcbbbbbcbbbbbbba",
			"abbbcbbccbcccbbccbba",
			"abbcccbbbcccccbbbbba",
			"abbcccbcbbcccbbbcbba",
			"abbbbcbcbbbcbbcbbbba",
			"abbccbdbcbcbcbcbccba",
			"abbcdbbcbdccbbbcbcba",
			"abbbccbbcbbbbcccbcca",
			"abbccccbbcbcbbcbbbba",
			"abccccbbccbcccbccbba",
			"abccbbbccbbbcccbbbba",
			"abbbbcbcbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaabacaa..aaaaaaaaa",
			"abbbececf..gcbbccbba",
			"abb..........bhb.i.b",
			"acj.k........lbm.dbc",
			"abc........bbbnb.opc",
			"bqj........bnhhb.bca",
			"abbbbbbbb..bhbbb.i.b",
			"abhhnhhhbrrbnb...cba",
			"agcdbbbhhbbnhb....sa",
			"......bbbttbhbbb....",
			"...........cnhhb....",
			"af.........ccbhb..sa",
			"ab...........chb..ba",
			"abubbbb......bnb..ba",
			"abbhhhbbbrrbcchbbbca",
			"abhhbhhhhbbhhhnvvnba",
			"abbbbbbbbttcccbvnnba",
			"ab.u.u.u......bnbnca",
			"abbbbcccw..wbbbcbbna",
			"aaaaaaaaa..aaaaaaaaa",
		},
		{
			"aaaabacaa..aaaaaaaaa",
			"abbcxbxcg..gcbbcccba",
			"acb..............y.b",
			"abz..............ccc",
			"acc..A............Bb",
			"b.z..............bba",
			"acd..............y.c",
			"ac...............cba",
			"ag................sa",
			"....................",
			"....................",
			"afu...............sa",
			"ab..u.............ba",
			"acC...............ba",
			"ab................ba",
			"cc...............Dca",
			"acu.u...........DDca",
			"ab.C.E.C.......DnDba",
			"abbbbbccw..wdbcbccDa",
			"aaaaaaaaa..aaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abbdbbcbFFFFbbbcbbba",
			"abb..............cca",
			"abb..............cba",
			"acc..G...........cBa",
			"abc..............bba",
			"abc..............dca",
			"ab...............cba",
			"aF................Fa",
			"aF................Fa",
			"aF................Fa",
			"aFH...............Fa",
			"ab..H.............ca",
			"acH...............da",
			"ab................ba",
			"ab...............Dca",
			"acH.H...........Dhna",
			"ab.H...E.......DhDva",
			"abbbccccFFFFbcbcnvIa",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbcbbbbbbbbbba",
			"abbbdcbcccbbbbbcbbba",
			"abbbbbcbcbbbbcbcbbBa",
			"abccbbbccbbbbccbbbba",
			"abbbbcbbbbbbbbccbbba",
			"abbbcccbbbbbbbccbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbcbcbbbbbbbbbbba",
			"abbbbbbbbcccbbbdbbba",
			"abbbbbbbbcbcbdbbbbba",
			"abbbbbbbbbcbbbbbbbba",
			"abbbbcbbcccbbcbbbbba",
			"abbbccbbbbbbbcbbbbba",
			"abbbcccbbbbcbccbbbba",
			"abbccccbbbcbcccbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abcbbbbbbbbbbbbbcbba",
			"ac.J.........K....ba",
			"ab................ca",
			"aK................Ba",
			"ab...ddd..........ba",
			"ab..d.d...........ba",
			"ac................ba",
			"ab........dd......ba",
			"abL......ddd......ba",
			"ab.......dd.......ba",
			"ab................ba",
			"ab................ba",
			"abL...............ba",
			"aMK...............ba",
			"aMN...............ba",
			"ab.O........d.....ca",
			"ab..NKP....ddd....ca",
			"abbbQQbQbbKbbbbbbbba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abccbbbbbbbbbbbbcbba",
			"acR.J........K..JSca",
			"ab................ca",
			"acL...............Ba",
			"ab...dd...........Ka",
			"ab................ba",
			"ab...............Kba",
			"ab................ba",
			"ac........d.......ca",
			"ab................ca",
			"ab...............Tba",
			"ac................ca",
			"abL...............ba",
			"ab...............Kba",
			"acL...............ba",
			"ac................ca",
			"ac..........d...PSba",
			"abcbbQcbbbbcbbKbbbba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abbbbbbbbbbbbbbbbbba",
			"acK.............J.ba",
			"ab................ba",
			"abL...............Ba",
			"ac................ba",
			"ab................ba",
			"ac................ba",
			"ac...............Kba",
			"ab................ba",
			"ab................ca",
			"aK................ca",
			"ac................ba",
			"aM................ba",
			"acL...............ba",
			"aM................ba",
			"ac................ba",
			"a..P.P..........P.ba",
			"acccbbQbcbcccbbbccba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abbbbbbbbbbbbbbbbbba",
			"ac.........K...K..ba",
			"ab................ba",
			"abL...............Ba",
			"ab................ba",
			"ac................ba",
			"ac................ba",
			"ac................ba",
			"ab................ba",
			"ab................ba",
			"ab................ba",
			"ab................ba",
			"ac...............Kba",
			"ab................ba",
			"aK................ba",
			"a.................ba",
			"a......P..........ba",
			"ac..ccbbcccbbcbbbbba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abbbbbbbbbbbbbbbbbba",
			"ab................ba",
			"ab................ba",
			"abL...............Ua",
			"ab................ba",
			"abK...............ba",
			"ab................ba",
			"ac................ba",
			"ac................ba",
			"ab................ba",
			"ab................ba",
			"ab................ba",
			"ab................ba",
			"ac................ba",
			"ac................ba",
			"a.................ba",
			"a...K.............ba",
			"a...cbcbbbbbbcb.bbba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"accbbbbbbbbbbbbbbbba",
			"abccbbbbbbbbbbbbbbba",
			"abbbbccbbbbbbbbbbbba",
			"abcbbcbbbbbbbbbbbbba",
			"abbbbcbbbbbbbbbbbbba",
			"abbbbbbbbbbbbbbbbbba",
			"abcbcbbcbcbbbbbbbbba",
			"abKccccbbbbbbbbbbbba",
			"a....cbcbccbbbbbbbba",
			"a...ccbccbbbbbbbbbba",
			"a...cbbbcbbbbbbbbbba",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"accb................",
			"a...c...............",
			"a..Vc...............",
			"a..cc...............",
			"aaaaa...............",
		},
		{
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"abb.................",
			"a.Jc................",
			"a..b................",
			"a..c................",
			"aaaa................",
		},
		{
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"....................",
			"accc................",
			"aWWc................",
			"aWWc................",
			"aaaa................",
		},
	}
}
