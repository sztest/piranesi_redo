return {
	noscore = true,
	disallow_changes = true,
	legend = {
		["a"] = {name = "piredo_terrain:default__dirt_with_grass"},
		["b"] = {name = "piredo_terrain:default__dirt"},
		["c"] = {name = "piredo_terrain:default__silver_sandstone_brick"},
		["d"] = {param2 = 3, name = "piredo_terrain:xdecor__stone_rune"},
		["e"] = {param2 = 2, name = "piredo_terrain:xdecor__stone_rune"},
		["f"] = {name = "piredo_terrain:wool__cyan"},
		["g"] = {name = "piredo_terrain:wool__white"},
		["h"] = {name = "piredo_terrain:default__silver_sandstone_block"},
		["i"] = {name = "piredo_terrain:quartz__chiseled"},
		["j"] = {name = "piredo_terrain:default__mossycobble"},
		["k"] = {name = "piredo_terrain:default__cobble"},
		["."] = {name = "piredo_terrain:ambient"},
		["l"] = {param2 = 2, name = "piredo_terrain:default__tree"},
		["m"] = {name = "piredo_terrain:default__tree"},
		["n"] = {param2 = 3, name = "piredo_terrain:default__tree"},
		["o"] = {name = "piredo_terrain:quartz__pillar"},
		["p"] = {param2 = 1, name = "piredo_terrain:quartz__pillar"},
		["q"] = {name = "piredo_terrain:default__grass_2"},
		["r"] = {name = "piredo_terrain:flowers__tulip_black"},
		["s"] = {param2 = 1, name = "piredo_terrain:default__tree"},
		["t"] = {name = "piredo_terrain:default__grass_4"},
		["u"] = {name = "piredo_terrain:flowers__viola"},
		["v"] = {name = "piredo_terrain:flowers__tulip"},
		["w"] = {name = "piredo_terrain:default__sapling"},
		["x"] = {name = "piredo_terrain:default__grass_3"},
		["y"] = {name = "piredo_terrain:default__grass_5"},
		["z"] = {name = "piredo_terrain:flowers__geranium"},
		["A"] = {param2 = 3, name = "piredo_terrain:xdecor__ivy"},
		["B"] = {name = "piredo_terrain:piranesi__candle"},
		["C"] = {param2 = 2, name = "piredo_terrain:xdecor__ivy"},
		["D"] = {name = "piredo_terrain:flowers__rose"},
		["E"] = {name = "piredo_terrain:default__leaves"},
		["F"] = {param2 = 4, name = "piredo_terrain:xdecor__ivy"},
		["G"] = {name = "piredo_terrain:default__bookshelf"},
		["H"] = {name = "piredo_terrain:xdecor__chair"},
		["I"] = {param2 = 3, name = "piredo_terrain:quartz__pillar"},
		["J"] = {param2 = 3, name = "piredo_terrain:xdecor__chair"},
		["K"] = {name = "piredo_terrain:luxury_decor__kitchen_wooden_table"},
		["L"] = {param2 = 1, name = "piredo_terrain:xdecor__chair"},
		["M"] = {param2 = 3, name = "piredo_terrain:doors__door_wood_a"},
		["N"] = {param2 = 2, name = "piredo_terrain:doors__door_wood_c"},
		["O"] = {param2 = 2, name = "piredo_terrain:xdecor__chair"},
		["P"] = {param2 = 1, name = "piredo_terrain:xdecor__cabinet"},
		["Q"] = {param2 = 3, name = "piredo_terrain:luxury_decor__simple_dark_green_armchair_with_green_pillow"},
		["R"] = {name = "piredo_terrain:xdecor__table"},
		["S"] = {param2 = 1, name = "piredo_terrain:luxury_decor__simple_dark_green_armchair_with_green_pillow"},
		["T"] = {name = "piredo_terrain:flowers__chrysanthemum_green"},
		["U"] = {param2 = 2, name = "piredo_terrain:quartz__pillar"},
		["V"] = {name = "piredo_terrain:quartz__block"},
		["W"] = {param2 = 2, name = "piredo_terrain:doors__door_wood_b"},
		["X"] = {param2 = 2, name = "piredo_terrain:doors__door_wood_a"},
		["Y"] = {name = "piredo_terrain:flowers__dandelion_yellow"},
		["Z"] = {name = "piredo_terrain:xdecor__stonepath"},
		["0"] = {param2 = 3, name = "piredo_terrain:xdecor__stonepath"},
		["1"] = {param2 = 1, name = "piredo_terrain:xdecor__stonepath"},
		["2"] = {param2 = 2, name = "piredo_terrain:xdecor__stonepath"},
		["3"] = {name = "piredo_terrain:piranesi__barrier"},
		["4"] = {param2 = 3, name = "piredo_terrain:doors__hidden"},
		["5"] = {param2 = 1, name = "piredo_terrain:doors__hidden"},
		["6"] = {name = "piredo_terrain:realchess__chessboard"},
		["7"] = {param2 = 5, name = "piredo_terrain:xdecor__ivy"},
		["8"] = {name = "piredo_terrain:default__glass"},
		["9"] = {param2 = 2, name = "piredo_terrain:doors__hidden"},
		[","] = {name = "piredo_terrain:default__apple"},
		["/"] = {name = "piredo_terrain:morelights_vintage__lantern_c"},
		[";"] = {name = "piredo_terrain:stairs__stair_dungeon_stone"},
		["'"] = {param2 = 4, name = "piredo_terrain:morelights_vintage__lantern_w"},
		["["] = {name = "piredo_terrain:piranesi__black"},
		["]"] = {name = "piredo_terrain:default__silver_sandstone"},
		["`"] = {param2 = 5, name = "piredo_terrain:morelights_vintage__lantern_w"},
		["-"] = {param2 = 2, name = "piredo_terrain:xdecor__moonbrick"},
		["="] = {name = "piredo_terrain:xdecor__moonbrick"},
		["<"] = {name = "piredo_terrain:piranesi__barrier", param2 = 2},
	},
	nodes = {
		{
			"aabababaaabbcccccccccccccccccaabababab",
			"abababababaccccccccccccccccccbbabababa",
			"aaaaaaaabaabcdeecccccacccccccabaabbbba",
			"abaaaaaaaaabdccceccccccccccccaaaaaabba",
			"baaaaaaaaaabcccccccccccccccccaaaaabbab",
			"abaaaaaaaaabcececccccccccccccaaaaaaaba",
			"baaaaaaaaaabcccccccacccccccccaaaaaaaab",
			"abaaaaaaaaabcccccccccccccccccaaaaaaaba",
			"baaaaaaaaaabcccccccccacccccccaaaaaaaab",
			"abaaaaaaaaabcccccccccccccaaccaaaaaaaba",
			"abaaaaaaaabbcccccccccccccacccaaaaaabaa",
			"aabaaaaabbbbccccccaacccccccacaaaaaaaba",
			"aaabaaabbbbbcccccccaccccfgfccaaaaaaaab",
			"aabaaaaabbbbcaccccccccccgfgccaaaaaaaba",
			"abaaaaaabbbbcccccccccaccfgfccaaaaaaaab",
			"baaaaaaaaaabcccccccccccacccccbaaaaaaba",
			"abaaaaaaaaacccccchcccchcccccccbaaaaaab",
			"aabaaaaaaaaaaabbbabiibaaaaaabbbaaaaaba",
			"abaaaaaaaaaaaaabaaaajaaaaaaaaaaaaaaaab",
			"aabaaaaaaaaaaaaaaajaaaaaaaaaaaaaaaaaba",
			"abaaaaaaaaaaaaaaaaakaaaaaaaaaaaaaaaaab",
			"babaaaaaaaaaaaaaaajaaaaaaaaaaaaaaaabba",
			"abaaaaaaaaaaaaaajajaaaaaaaaaaaaaaaabaa",
			"baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaba",
			"abaaaaaaaaaaaajakaaaaaaaaaaaaaaaaaaaab",
			"baaaaaaaaaaaakaaaaaaaaaaaaaaaaaaaaaaba",
			"abaaaaaaaaaaajaaaaaaaaaaaaaaaaaaaaaaab",
			"baaaaaaaaaaajaaaaaaaaaaaaaaaaaaaaaaaba",
			"abaaaaaaaaajaaaaaaaaaaaaaaaaaaaaaaaaab",
			"baaaaaaaaaaajaaaaaaaabbaaaaaaaaaaaaaba",
			"abaaaaaaaaaajaaaaaaaabbaaaaaaaaaaaabaa",
			"baaaaaaaaaajaaaaaaaaabbbaaaaaaaaaaabba",
			"abaaaaaaaaakajaaaaaaabbaaaaaaaaaaaabab",
			"baaaaaaaaaaaakaaaaaaaaaaaaaaaaaaaababa",
			"abaaaaaaaaaajaaaaaaaaaaaaaaaabaaaaabab",
			"aabababababaaababababababababababababa",
			"ababababababaajbababababababababababaa",
			"aaaaaaaaaaaabjajaaajaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaabaaajjaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaabbbbbbaaaaaaaaaaaaaaaaaa",
		},
		{
			"<<l<m<l<<<nccccccho<<ph<<<<<c<qnrs<l<n",
			"<l.l.l.l.s.......hhhhhh.....cmntsulvs<",
			"m.......l..c.....h....h.....c...ux.ab<",
			"<n.........c.....h....h.....c.uy.y.as<",
			"n....xyr.tqc.....h....h.....c........n",
			"<s...qzqt.xc.....hA...h.....c.......m<",
			"s....q..t.xc....Bh....h.....c........n",
			"<mz........cccccch...Chcccccc.......l<",
			"mDty.......cE.F..h....hGFGGGc..xxy...n",
			"ws.D....qyyc..H..I....I.....c..x....s<",
			"<m..zx..tyacJKKL.M....N.....c..y.q.l.<",
			"<.s.yqx.aaacJKEL.IA...E.....c.......l<",
			"<..l.zDaaaac..O..h....E.....c........s",
			"<.m.xyy.aaacA...Ph....hQ.R.Sc..qy...m<",
			"<n..z.t.aaac...EEh....h.....c.qxt.v..m",
			"m....zx.tDTc.EEEEhU..Uh.....ca.yyyx.m<",
			"xntut......Vccccccc..cccccccVaaxTy...l",
			"rTlty...tr.rEEaaa.oWXo.EEEt.aaaxxt.ym<",
			"yltqy.yq...E.Y.a..FZFE...EuqtqqDyqqT.s",
			"xts.y..tDx.E.rxyv..zEEu..vEt.x.q.yvYn<",
			"xs.Ty.y.t.EEq.xty.Z.ErvDqquyy.tq..qt.s",
			"m.q.......Ertvy.v..0ztqq...yzDT..y..n<",
			"<l....tvqYTyzxyv.1.t.D...xY.yxtxy..n.<",
			"l.......qv.xyyqZ.u..tDxTTqyt..DyYu..n<",
			"<sY.yT.xxrtytt....xtxyqqxxuyt.qtDt.q.n",
			"n.xqxYq.x.u.Z.1...qtyxxYt....Yq..yxrn<",
			"<lYqyDx..ytq..yyyDYxqqt.t.T.tTrrtqyz.l",
			"m..qqy.ry.qD.0txDqqvqq.qqxq..qyT....m<",
			"<s.yqvt.uxv.Z.t.Dr..q.T.xttx.xTxy....m",
			"s..yqyq.qyYt.xztDYxqtaatt.txYqyyTuy.n<",
			"<m...v..t.vZ.1xr..uqxaaxuq.Y.Y.....s.<",
			"m..x.t..ytq.t.Ty.xytyaaa.z...Yzz.ty.n<",
			"<lyyrrqTy.y.2.ztv..yyaa.yxDx.xuu.yyr.l",
			"l..tt.yx.xtu..yYxuuD...xvDx...qy..xxl<",
			"<styyxyv.yTq.1.xxxt..YrqtD.x.....yqu.s",
			"<.s.n.n.m.n333l.n.n.s.l.n.n.l.m.m.s.l<",
			"<s.n.m.s.s.m0..m.n.s.n.s.s.m.n.n.n.l.<",
			"<...........m.0...1..................<",
			"<............m..2....................<",
			"<<<<<<<<<<<<<<mmnnmm<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<l<m<l<<<nccccccho<<ph<<<<<c<<n<s<l<n",
			"<l.l.l.l.s.......hhhhhh.....cmm.s.l.s<",
			"m.......l..c.....h....h.....c.......a<",
			"<n.........c.....h....h.....c.......s<",
			"n..........c.....h....h.....c........n",
			"<s.........c.....hA...h.....c.......m<",
			"s..........c.....hA...h.....c........n",
			"<m.........cccccch...Chcccccc.......l<",
			"m..........cEFF..h....hGGFGFc........n",
			"<s.........c.....I....I.....c.......s<",
			"<m.........c.....4....5.....c......l.<",
			"<.s........c.....IA...E.....c.......l<",
			"<..l.......c.....h...Ch.....c........s",
			"<.m........cA...Ch....h..6..c.......m<",
			"<n.........c.....h7...h.....c........m",
			"m..........c....EhU..UhE....c.......m<",
			"<n.........V.E8c8cc..ccEc8c8V........l",
			"<.l...........F...o59o.E............m<",
			"<l................FFFE...............s",
			"<.s.................................n<",
			"<s...................................s",
			"m...................................n<",
			"<l.................................n.<",
			"l...................................n<",
			"<s...................................n",
			"n...................................n<",
			"<l...................................l",
			"m...................................m<",
			"<s...................................m",
			"s...................................n<",
			"<m.................................s.<",
			"m...................................n<",
			"<l...................................l",
			"l...................................l<",
			"<s...................................s",
			"<.s.n.n.m.n333l.n.n.s.l.n.n.l.m.m.s.l<",
			"<s.n.m.s.s.n...m.n.s.n.s.s.m.n.n.n.l.<",
			"<...........m........................<",
			"<............m.......................<",
			"<<<<<<<<<<<<<<mmnnmm<<<<<<<<<<<<<<<<<<",
		},
		{
			"<E,EmEEEsEncccccchiiiih<<<<<c<<n<EEs<n",
			"EEEEElEEEs.c.....hhhhhh.....cmn.sEEEs<",
			"EEEEEEEEEEsc.....h....h.....cEE.EEEEEs",
			"EEEEEEEEEEEc.....h....h.....cEEEE.EEEE",
			"EEEEEEE.EEEc.....hA...h.....c.....EEEE",
			"EsEEE......c.....hA...h.....c....EEEmE",
			"sEsEE......c.....hA..Ch.....c....EEmEn",
			"EsEEE......cccccchA..Chcccccc....EEE,E",
			"EEEEE......c/FFF/h....h/GGG/c....EEEEE",
			"EsEEE......c.....i....i.....c.....EEsE",
			"EEEEE......c.....i/../i.....c....EEEEE",
			"EEEE.......c.....iA..Ci.....c....EEElE",
			"EEEE.......c....ChA..Ch.....c....EElEs",
			"nEEEE......cA...Ch....h.....c....EEEmE",
			"<EE,E......cA....h7../h.....c....EEEEm",
			"EE,EE......c.....hiiiiEE....c.....EEEE",
			"EnElEE.....V8.8c8ccccE..c8c8V....EEEEE",
			"EElEEE.....F..F.F.iiiE...........EEElE",
			"El..EE............FFF............EEEEs",
			"EEsEEE...........................EEEEE",
			"EEEEEE...........................EEEEE",
			"EE.EE............................EEEEE",
			"EEEE..............................EEEE",
			"lEEE..............................EEEE",
			"EsEEE............................EEEEE",
			"EEEEE............................EEEn<",
			"E,EEE.............................EEEn",
			"sEEEE.............................EEEE",
			"EEEEE..............................EEE",
			"sEEEE.............................EEEE",
			"EmEE.............................EEsE,",
			"mElEE............................E,EnE",
			"<lE,EEEEEEEE...EEEEEEEEEEEEE.....EEEEn",
			"lEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEElE",
			"E,EEEEEEEEEEEEEEEEE,EEEEEnEEEEEEEEEEEs",
			"sEEEEEEEEEEEEE,EEEsEsEEEsEEEEEnEmEEEEE",
			"<sEEEEEEEsE,EEEEEEEEEEEEEsEmEn.EEnEEEE",
			"<.EEEEEEEE,EnE..EEEEEEEEEEEEEEnEEEEEEE",
			"E.EEEEEEEEEEEEEEEEEEEEEEEEEEEEE,EEEEEE",
			"<EE<EEEE<EEEEEEEE<EEE<<<EEE<EEEEE<<<<<",
		},
		{
			"lEEEEEEEEEscccccccccccc<<<<<cEE,EsEEEs",
			"EEEEEEEEEnEcccccccccccc.....cEEEEEEEnE",
			"EEEEEElEEEEcccccccccccc.....cEEEEEEEEE",
			"EEEEEEEEEEEcccccccccccc.....cEEEEEEEEE",
			"nEnEEEEE.EEcccccccc..cc.....cEEEEEEEEs",
			"EnEEE..EEE.cccccccc..cc.....c....EEE,E",
			"EEEE.......cccccccccccc.....c.....EEEE",
			"EEEEE......cccccccccccccccccc....EEEEE",
			"EEEE.......cccccccccccccccccc....EEEE,",
			"EEEEE......cccccccccccccccccc...EEEEEE",
			",EEEEE.....cccccccccccccccccc...E.EEEE",
			"EEEE,EE....cccccccccccccccccc...EEEEEE",
			"EEEEEEE....cccccccccccccccccc....EEEEE",
			"EElEEEE....cccccccccccccccccc....EEEEE",
			"EEEEEE.....cccccccccccccccccc....EEEEE",
			"EEEEE......ccccccccccEEcEcccc....E.EEE",
			"ElEEE......Vccccchccc..c.cccV....EEEEE",
			"EEEEEE.....F;.;.;'.EV..;.;.......EEEEE",
			"EEElE.............................EEEE",
			"EEEEEE...........................EEEEE",
			"EsEEE............................EEEEE",
			"EEEEE...........................EEEEEE",
			"ElEEE...........................E,EEEE",
			"lEEE............................EEEEEE",
			"EEEEE............................EEEEE",
			"EEEE.............................EEEEE",
			"EEEEE............................EEEEE",
			"EEEE.............................EEEmE",
			"EEEEE............................EEmEE",
			"EEEEE...........................EEEE,E",
			"EEEEE...........................EEEEEE",
			"EEEEE...........................EEEEEE",
			"EEEEEEE.E..EEEEE..E...E.E.EEEEEEEEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"EEnEEEEEEEEEEEEEEEEEEEEEEEsElEEEEEEEEE",
			"EEEEEEEEEEEEEEE,EEEsEEEEEEEEEEEmEsEEEl",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"<EEEEE<E<E<E<<<EEE<E<EEEEEEE<E<EEEEE<<",
		},
		{
			"EEEEEEEEEEEcccccc<[[[[[[[[[[cEEEEEEEEE",
			"EEEEEEEEEEEcccccc.[[[[[[[[[[cEEEsEEEEE",
			"EEEEEEEEEEEcccccc.[[[[[[[[[[cEEsEEEEEE",
			"EEEEEEEEEEEcccccc.[[[[[[[[[[cEEEEEEEEE",
			"nEEE..EEEE.cccccc.[..[[[[[[[c.E.E.EsEE",
			"EEEE....E..cccccc.[..[[[[[[[c.....EEEE",
			"EEEE.......cccccc.[[[[[[[[[[c.....EEEE",
			"EEEE.......cccccccccccccccccc.....EEEE",
			"EEEE.......c................c.....EEEE",
			"EEEEE......c................c....EEEEE",
			"EEEEEE.....c................c...EEEsEE",
			"EEEEEE.....c................c....EEEEE",
			"EEEEEEE....c................c....EEEEE",
			"EEEEEE.....c................c.....EEEE",
			"EEEEE......c................c.....EEEE",
			"EEEEE......c.EE......E....EEc....EEEEE",
			"EEEEE.....CVccccccccc.cccc..V.....EEEE",
			"EEEEE......F................F.....EEEE",
			"EEEEE.............................EEEE",
			"EEEEE.............................EEEE",
			"EEEEE............................EEEEE",
			"sEEE.............................EEEEE",
			"EEEEE...........................EEEEEE",
			"EEEE.............................EnEEE",
			"EEEE.............................EEEEE",
			"EEEE..............................EEEE",
			"EEEE..............................EmEE",
			"EEEE.............................EEEEE",
			"EEEE.............................EEEEm",
			"EEEE.............................EEEEE",
			"EEEEE...........................EEEEEE",
			"EEEE.............................EEEEE",
			"EEEEE.......E.E.............E.E.EEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"EEEEEEEEEEEEElEEEEEEEEEEEEElEmEEEEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"EEEEEEEEEEEEEEEEnEEEnEsEEEEEEEEEnEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"<<<E<<<<<<<<<<<<<E<<<E<E<<<E<<<<<E<<<<",
		},
		{
			"EEEEEEEEEEEc<<<<<<[[[[[[[[[[cEEEEEEEEE",
			"EEEEEEEEEEEc......[..[.....[cEEEEEEEEE",
			"EEEE.EEEEEEc......[.....[..[cEEEEEEEEE",
			"EEEE..EEEEEc......[[[[.....[c.EEEEEEEE",
			"EEEE...EE..c......[..[.....[c.....EEEE",
			"EEE........c......[..[.....[c......EEE",
			"EE.........c......[[[[[[[[[[c......EEE",
			"EEE........]]]]]]]]]]]]]]]]]c......EEE",
			"EEE........c................c.....EEEE",
			"EEEE.......c................c....EEEEE",
			"EEEEE......c................c....EEEEE",
			"EEEEEE.....c................c....EEEEE",
			"EEEEEE.....c................c.....EEEE",
			"EEEEEE.....c................c......EEE",
			"EEEEE......c................c.....EEEE",
			"EEEE.......c.E.....EEE.....Ec.....EEEE",
			"EEEE......CV8c8c8cc...c8c8c.V.....EEEE",
			"EEEE.......F............F...F......EEE",
			"EEEE................................EE",
			"EEEE...............................EEE",
			"EEEE...............................EEE",
			"EEEE..............................EEEE",
			"EEEE.............................EEEEE",
			"EEEE.............................EEEEE",
			"EEE...............................EEEE",
			"EEE................................EEE",
			"EEE................................EEE",
			"EEE...............................EEEE",
			"EEE...............................EEEE",
			"EEEE.............................EEEEE",
			"EEEE.............................EEEEE",
			"EEEE..............................EEEE",
			"EEE...............................EEEE",
			"EEEEEE.....EEEEE...........EEEEEE..EEE",
			"EEEEEEE.E.EEEEEEEEE.EEEEE.EEEEEEEEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"<EEEEE.E...EEEEEEEEEEEEEEEEEEEEEEEEEEE",
			"<.EE............EEE.EEEE..EEE...EEEEE<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<EEE<<<E<<<c<<<<<<[[[[[[[[[[cEE<EEEEE<",
			"<.E...EEE..c......[..[.....[cEEEEEEE.<",
			"<......EEE.c......[.....[..[c.E.E.E.E<",
			"EEE.....E..c......[[[[.....[c......EEE",
			"EE.........c......[..[.....[c.......EE",
			"E..........c......[..[.....[c........E",
			"<..........c......[[[[[[[[[[c.......EE",
			"E..........]]]]]]]]]]]]]]]]]c........<",
			"EE.........c................c.......E<",
			"<..........c................c......EEE",
			"<..........c................c......EE<",
			"<.EE.......c................c......E.<",
			"<.EEE......c................c........<",
			"<EE........c................c........<",
			"<.E........c................c.......E<",
			"EE.........c..E....EEE......c......EEE",
			"EEE.......CV8c8c8cc.8cc8c8c8V........<",
			"<E.........F...F........F...F........<",
			"EE...................................<",
			"<E...................................<",
			"EE...................................<",
			"EE.................................E.<",
			"EEE...............................EEE<",
			"EE.................................EEE",
			"E...................................E<",
			"EE...................................<",
			"E...................................E<",
			"EE.................................EEE",
			"E...................................EE",
			"EE.................................EEE",
			"EEE...............................EEE<",
			"<E.................................E.<",
			"E....................................<",
			"EE...................................E",
			"EEE.E.......E.E.............E.E.E...EE",
			"<EEEEE.....EEEEE.E...E.E...EEEEEEE.E.<",
			"<.EEE.........E..EE..EEEE.EEE.E.EEEE.<",
			"<..E.............E...E.E...E.......E.<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<<c<<<<<<[[[[[[[[[[c<<<<<<<<<",
			"<..........c......[..[.....[c........<",
			"<..........c......[.....[..[c........<",
			"<..........c......[[[[.....[c........<",
			"<..........c......[..[.....[c........<",
			"<..........c......[..[.....[c........<",
			"<..........c......[[[[[[[[[[c........<",
			"<..........]]]]]]]]]]]]]]]]]c........<",
			"<..........c................c........<",
			"<..........c................c........<",
			"<..........c................c........<",
			"<..........c................c........<",
			"<..........c................c........<",
			"<..........c................c........<",
			"<..........c................c........<",
			"<..........c`E`.`..EE..`.`.`c........<",
			"<.........CVc.ccccc88cccccccV........<",
			"<..........F;..F;......;F;.;F........<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<-c<<<<<<[[[[[[[[[[c-<<<<<<<<",
			"<.........=c......[[[[[[[[[[c=.......<",
			"<.........=c......[[[[[[[[[[c=.......<",
			"<.........=c......[[[[[[[[[[c=.......<",
			"<.........=c......[[[[[[[[[[c=.......<",
			"<.........=c......[[[[[[[[[[c=.......<",
			"<.........=c......[[[[[[[[[[c=.......<",
			"<.........=]]]]]]]]]]]]]]]]]c=.......<",
			"<.........=c................c=.......<",
			"<.........=c................c=.......<",
			"<.........=c................c=.......<",
			"<.........=c................c=.......<",
			"<.........=c................c=.......<",
			"<.........=c................c=.......<",
			"<.........=c................c=.......<",
			"<.........=c.E..E..EE.E.....c=.......<",
			"<.........=Vc.8c.cc88c.cc8ccV=.......<",
			"<.........=F...F....F...F.F.F=.......<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<<-<<<<<<<<<<<<<<<<-<<<<<<<<<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=cccccccccccccccc=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=................=........<",
			"<..........=....EEEEE.EEE...=........<",
			"<..........=cc8cc..88c..c8cc=........<",
			"<..........=...F....F...F.F.=........<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<<<-<<<-<<<<<<-<<<-<<<<<<<<<<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=cccccc=ccc=.........<",
			"<...........=ccc=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=...=......=...=.........<",
			"<...........=.`.=..EE..=.`.=.........<",
			"<...........=ccc=cc88cc=ccc=.........<",
			"<...........E.;F=..FF..EF;F=.........<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<<<<-<-<-<<<<-<-<-<<<<<<<<<<<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=c=.=....=.=c=..........<",
			"<............=c=.=cccc=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=....=.=.=..........<",
			"<............=.=.=.``.=.=.=..........<",
			"<............=c=.=cccc=.=c=..........<",
			"<............=.=.E.;;.=.EF=..........<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<<<<<-<<<-<<-<<<-<<<<<<<<<<<<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=cc=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=..=...=...........<",
			"<.............=...=cc=...=...........<",
			"<.............=...E..=...=...........<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<<<<<<<<<<--<<<<<<<<<<<<<<<<<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................==................<",
			"<..................E=................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<....................................<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
		{
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
			"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
		},
	}
}
