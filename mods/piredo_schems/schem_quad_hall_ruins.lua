return {
	internal_name = "Quad Hall: Flooded",
	legend = {
		["a"] = {name = "piredo_terrain:default__silver_sandstone_brick"},
		["b"] = {name = "piredo_terrain:default__silver_sandstone"},
		["c"] = {name = "piredo_terrain:default__dirt_with_grass"},
		["."] = {name = "piredo_terrain:ambient"},
		["d"] = {param2 = 3, name = "piredo_terrain:quartz__pillar"},
		["e"] = {name = "piredo_terrain:quartz__pillar"},
		["f"] = {name = "piredo_terrain:default__silver_sandstone_block"},
		["g"] = {name = "piredo_terrain:default__grass_4"},
		["h"] = {name = "piredo_terrain:xdecor__cobweb"},
		["i"] = {name = "piredo_terrain:default__grass_1"},
		["j"] = {name = "piredo_terrain:default__grass_3"},
		["k"] = {param2 = 2, name = "piredo_terrain:xdecor__ivy"},
		["l"] = {name = "piredo_terrain:default__water_flowing"},
		["m"] = {param2 = 1, name = "piredo_terrain:default__water_flowing"},
		["n"] = {param2 = 2, name = "piredo_terrain:default__water_flowing"},
		["o"] = {param2 = 3, name = "piredo_terrain:default__water_flowing"},
		["p"] = {param2 = 4, name = "piredo_terrain:default__water_flowing"},
		["q"] = {param2 = 1, name = "piredo_terrain:quartz__pillar"},
		["r"] = {param2 = 5, name = "piredo_terrain:default__water_flowing"},
		["s"] = {name = "piredo_terrain:default__grass_2"},
		["t"] = {name = "piredo_terrain:default__grass_5"},
		["u"] = {param2 = 6, name = "piredo_terrain:default__water_flowing"},
		["v"] = {param2 = 7, name = "piredo_terrain:default__water_flowing"},
		["w"] = {param2 = 5, name = "piredo_terrain:xdecor__ivy"},
		["x"] = {param2 = 2, name = "piredo_terrain:quartz__pillar"},
		["y"] = {param2 = 4, name = "piredo_terrain:xdecor__ivy"},
		["z"] = {param2 = 15, name = "piredo_terrain:default__water_flowing"},
		["A"] = {name = "piredo_terrain:default__dirt"},
		["B"] = {param2 = 14, name = "piredo_terrain:default__water_flowing"},
		["C"] = {param2 = 3, name = "piredo_terrain:xdecor__ivy"},
		["D"] = {name = "piredo_terrain:quartz__chiseled"},
		["E"] = {name = "piredo_terrain:morelights_vintage__lantern_c"},
		["F"] = {name = "piredo_terrain:default__water_source"},
		["G"] = {param2 = 12, name = "piredo_terrain:default__water_flowing"},
		["H"] = {param2 = 13, name = "piredo_terrain:default__water_flowing"},
	},
	nodes = {
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaabbaaaaaaaaaa",
			"aaaaaaaabacaaaaaaaaa",
			"aaaaaaaaacaaaaaaaaaa",
			"aaaacaaaaaaaaaaaaaaa",
			"aaaccaaaaaaaaaaaaaaa",
			"aaacccaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaa..aaaaaaaaa",
			"aaaaaaaad..efaaaaaaa",
			"aaaaaaafg..hfaaaaaaa",
			"aaaaaaaf...hfaaaaaaa",
			"aaaaaaaf..i.faaaaaaa",
			"aaaaaaaf....faaaaaaa",
			"aaafaaafj..kfaaaaaaa",
			"affffff...lmfffffffa",
			"ae....j..lmnoponmgqa",
			"........lmnoprponmj.",
			".s....tlmnoprurpong.",
			"ad.j..lmnopruvurpwqa",
			"afffffffnoprfffffffa",
			"aaaaafffoprufrurpona",
			"aaaaaaafpruvfuvurpoa",
			"aaaaaaafpruvfuurpona",
			"aaaaaaafoprufvurpona",
			"aaaaaaafioprfurponma",
			"aaaaaaffxgtxfrponmla",
			"aaaaaaaaa..aaaaaaaaa",
		},
		{
			"aaaaaaaaa..aaaaaaaaa",
			"aaaaaaafe..efaaaaaaa",
			"aaaaaaaf....faaaaaaa",
			"aaaaaaaf....faaaaaaa",
			"aaaaaabh...hbaaaaaaa",
			"aaaaafbf....faaaaaaa",
			"aaafffff...kfaaaaaaa",
			"affy.f.f.....ffffffa",
			"ae.....y..........qa",
			"....................",
			"....................",
			"ad...........z..wwqa",
			"affff.......bffffffa",
			"aaaafhfb....fAf....a",
			"aaaaaffb...Bvfz....a",
			"aaaaaaafC..zff.....a",
			"aaaaaaaf....fz.....a",
			"aaaaaaaf....f......a",
			"aaaaaaffx..xf......a",
			"aaaaaaaaa..aaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaafDDDDfaaaaaaa",
			"aaaaaaafE..Efaaaaaaa",
			"aaaaaaaf....faaaaaaa",
			"aaaaaf.f....faaaaaaa",
			"aaaafhff....baaaaaaa",
			"aaafh.ff...kfaaaaafa",
			"affy...fE..kfffffffa",
			"aDE....y....hh...EDa",
			"aD................Da",
			"aD................Da",
			"aDE..........B.wwwDa",
			"afffh..fE..Ehvfffffa",
			"aaafh..f....fFff...a",
			"aaaffffb....BvBf...a",
			"aaaaaaafC..Gruff...a",
			"aaaaaaafC...fHf....a",
			"aaaaaaafE..Efff....a",
			"aaaaaaafDDDDfaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
		},
		{
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaabaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
			"aaaaaaaaaaaaaaaaaaaa",
		},
	}
}
