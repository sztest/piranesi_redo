-- LUALOCALS < ---------------------------------------------------------
local ipairs, loadfile, minetest, pairs, piredo_api, string, tonumber,
      type
    = ipairs, loadfile, minetest, pairs, piredo_api, string, tonumber,
      type
local string_match
    = string.match
-- LUALOCALS > ---------------------------------------------------------

local function deepcopy(x)
	if type(x) == "table" then
		local t = {}
		for k, v in pairs(x) do t[k] = deepcopy(v) end
		return t
	end
	return x
end

local glow_drawtypes = {
	plantlike = true,
	airlike = true,
	signlike = true,
	torchlike = true,
	firelike = true,
}

local function alphaify(obj, alpha)
	if type(obj) == "string" then return "(" .. obj .. ")^[opacity:" .. alpha end
	if type(obj) == "table" then
		if obj.name then
			obj.name = alphaify(obj.name, alpha)
		end
		for i, v in ipairs(obj) do
			obj[i] = alphaify(v, alpha)
		end
	end
	return obj
end

local footstepgain = tonumber(minetest.settings:get("piranesi_footsteps")) or 1

local modname = minetest.get_current_modname()
loadfile(minetest.get_modpath(modname) .. "/exported.lua")(function(def)
		def = deepcopy(def)

		local orig = def._raw_name
		local suff = orig:gsub(":", "__")
		local myname = modname .. ":" .. suff

		def._raw_name = nil
		def.groups = def.groups or {}

		if def.alpha then
			alphaify(def.tiles, def.alpha)
			alphaify(def.specia_tiles, def.alpha)
			def.use_texture_alpha = "blend"
			def.alpha = nil
		end

		if def.drawtype == "airlike" or (def.walkable == false
			and not string_match(orig, "curtain")) then
			def.pointable = false
		end
		if def.drawtype == "flowingliquid" then
			def.liquid_alternative_flowing = myname
		end
		if def.drawtype == "allfaces_optional" then
			def.drawtype = "allfaces"
		end

		if def.type ~= "node" then
			def.type = "node"
			def.drawtype = "signlike"
			def.paramtype2 = "wallmounted"
			def.selection_box = {type = "wallmounted"}
			def.sunlight_propagates = true
			def.tiles = {def.inventory_image}
			def.walkable = false
		end

		def.use_texture_alpha = def.use_texture_alpha and "blend"
		if def.drawtype and def.drawtype ~= "normal" then
			def.paramtype = "light"
			def.use_texture_alpha = def.use_texture_alpha or "clip"
		end

		if def and def.node_box and def.node_box.type == "connected" then
			def.connects_to = {"group:connecty"}
			def.groups.connecty = 1
		end
		if piredo_api.starts_with(orig, "ropes:") then
			def.groups.connecty = 1
		end

		def.sounds = def.sounds or {}
		def.sounds.dug = def.sounds.dug
		or {gain = 0.25, name = "default_dug_node"}
		if def.drawtype == "firelike" then
			def.sounds.dug.name = "fire_small"
		end

		def.sounds.place = nil

		if not string_match(myname, "glass") then
			for _, v in pairs(def.sounds) do
				if v.name == "default_break_glass" then
					v.name = "default_dug_node"
					v.gain = 0.25
				elseif v.name and string_match(v.name, "glass") then
					v.name = "default_hard_footstep"
				end
			end
		end

		local spec = def.sounds.dig or def.sounds.footstep
		if spec then
			def.on_punch = function(pos)
				minetest.sound_play(spec.name,
					{pos = pos, gain = 0.5}, true)
			end
		end

		if type(def.sounds.footstep) == "table" then
			def.sounds.footstep.gain = (def.sounds.footstep.gain or 1) * footstepgain / 4
		elseif type(def.sounds.footstep) == "string" then
			def.sounds.footstep = {
				name = def.sounds.footstep,
				gain = footstepgain / 4
			}
		end

		if def.paramtype == "light" and (def.light_source or 0) < 3
		and glow_drawtypes[def.drawtype or "normal"] then
			def.light_source = 3
		end

		if def.liquid_move_physics then
			def.move_resistance = 3
		end

		if string_match(myname, "castle_tapestries") then
			def.walkable = false
		end
		if string_match(myname, "castle_masonry") then
			def.visual_scale = (def.visual_scale or 1) * 257/256
		end

		minetest.register_item(myname, def)
		minetest.register_alias(orig, myname)
	end)
