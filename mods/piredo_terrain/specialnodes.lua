-- LUALOCALS < ---------------------------------------------------------
local minetest
    = minetest
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

-- Ambient = air with ambient light

local ambnode = modname .. ":ambient"
minetest.register_node(ambnode, {
		drawtype = "airlike",
		pointable = false,
		walkable = false,
		sunlight_propagates = true,
		paramtype = "light",
		light_source = 3,
		buildable_to = true,
		air_equivalent = true,
	})
minetest.register_alias("piredo_map:ambient", modname .. ":ambient")

function minetest.remove_node(pos)
	return minetest.set_node(pos, {name = ambnode})
end

-- Void = bottomless pit

minetest.register_node(modname .. ":void", {
		drawtype = "normal",
		tiles = {"[combine:1x1^[noalpha"},
		post_effect_color = "#000000FF",
		walkable = false,
		groups = {piredo_player_void = 1}
	})
minetest.register_alias("mapgen_stone", modname .. ":void")
minetest.register_alias("mapgen_water_source", modname .. ":void")
minetest.register_alias("mapgen_river_water_source", modname .. ":void")
minetest.register_alias("piredo_map:void", modname .. ":void")

minetest.set_mapgen_setting("mg_flags", "nocaves,nodungeons", true)
minetest.set_mapgen_setting("mgflat_spflags", "nolakes,nohills", true)
