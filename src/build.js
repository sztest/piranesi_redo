'use strict';

/*****************************************************************\
 * 
 * Export translation data to Minetest locale files
 * 
 * Convert from format used by Weblate to the format that Minetest
 * can natively use for in-game strings.
 * 
\*****************************************************************/

process.on('unhandledRejection', err => { throw err; });

const fsp = require('fs')
	.promises;
const path = require('path');

const srcdir = path.join(__dirname, 'languages');
const srcraw = (async() => {
	const raw = await fsp.readFile(path.join(srcdir, 'en.json'));
	const strs = JSON.parse(raw.toString());
	return strs;
})();
const srckeys = (async() =>
	Object.fromEntries(Object.keys(await srcraw)
		.filter(x => !x.startsWith('clue:'))
		.map(x => [x, true])))();

const stats = {};
const jsonrx = /^([a-zA-Z0-9_]+)\.json$/;
fsp.readdir(srcdir)
	.then(async list => await Promise.all(list
		.filter(x => jsonrx.test(x))
		.map(async fn => {
			const langcode = fn.match(jsonrx)[1];
			const raw = await fsp.readFile(path.join(srcdir, fn));
			const strings = JSON.parse(raw.toString());
			const allkeys = await srcraw;
			const matched = Object.keys(strings)
				.filter(k => allkeys[k])
				.length;
			if(matched)
				stats[langcode] = matched;
			const allowkeys = await srckeys;
			const lines = Object.entries(strings)
				.filter(([k]) => allowkeys[k])
				.map(([k, v]) => `${k}=${v}`)
				.sort();
			if(!lines.length) return;
			if(langcode === 'en') return;
				lines.unshift('# textdomain: piredo_api');
			await fsp.writeFile(path.join(__dirname, '..', 'mods', 'piredo_api',
				'locale', `piredo_api.${langcode}.tr`), lines.join('\n'));
		})))
	.then(async () => {
		await fsp.writeFile(path.join(__dirname, '..', 'mods', 'piredo_api',
			'translated.lua'), `return {\n${
				Object.keys(stats).sort()
					.map(k => `\t${k} = ${stats[k]},\n`)
					.join('')
			}}\n`)
	});
