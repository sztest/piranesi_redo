const fs = require('fs');

const load = n => JSON.parse(fs.readFileSync(n, 'utf-8'));
const save = (n, o) => fs.writeFileSync(n, JSON.stringify(o, null, '    '));

const en = load('en.json');
const enKeys = Object.keys(en)
	.filter(x => x.startsWith('clue:'))
	.sort((a, b) => b.length - a.length);

for(const f of process.argv.slice(2)){
	const o = f.replace(/\.orig$/, '');
	if(o === f) throw 'not orig file';
	const idb = load(f);
	const odb = {}
	for(const k of Object.keys(idb).sort()){
		const nk = enKeys.find(x => x.startsWith(k)) || k;
		odb[nk] = idb[k];
	}
	save(o, odb);
}
