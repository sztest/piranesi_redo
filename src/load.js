'use strict';

/*****************************************************************\
 * 
 * Load new English translation source from raw data
 * 
 * Merge sources automatically:
 * - game.conf strings
 * - in-game strings detected at startup time, saved to a world
 * - clue strings, hard-coded into this script for now
 * 
\*****************************************************************/

process.on('unhandledRejection', err => { throw err; });

const fsp = require('fs')
	.promises;
const path = require('path');
const crypto = require('crypto');

if(process.argv.length < 3)
	throw `\n\nusage: ${process.argv0} ${process.argv[1]} ~/.minetest/worlds/...` +
		'\nmust point to a world that has started up at least once to generate the' +
		' translation template data.\n\n';

const cluetext = {
	clock: `It appears to be a device for inputting a series of times: one button changes the clock face, while the other, I believe, inputs it, as if you were entering a code.`,
	final: `Coins flee North, and will not touch the amulet.\n\nTime flees East, and will not touch the King.\n\nThe King flees South, and must be placed last.`,
	folklore: `When creating a compilation of local folklore, I came across an odd manuscript. Though the back of it was torn out, I still feel I should recount what was left of it, for it was extraordinarily old. It is titled "Piranesi's Victory". The story begins with three magician-princes and their jealous uncle. After the king dies, the uncle and his allies chase the princes from the throne room. Each prince flees in a different direction, running straight ahead until they tire. The first prince encounters a graveyard and summons an army of the undead. The second encounters a lake, and swims to an island in its center, and then enchants it to stop others from reaching it. The third encounters a tower, and casts a clever illusion to scare the army away. All three princes die, despite their efforts. No character is explicitly named "Piranesi", oddly enough … perhaps they were mentioned in the missing pages.`,
	forge: `I believe these strange boxes on the ground are furnaces, though I don't think all of them work. One that does, however, could provide great heat to an object that is placed in its center.`,
	machine: `I doubt the machine I found in the forest maze can be repaired, but I shall attempt to do so anyway.\n\nI will need some pewter to make new gears.\n\nRed, yellow, green, and blue — I have seen those colors before, in multiple places.`,
	manuscript: `and the fourth Prince, named Piranesi, ran South, and came upon an old mansion. He, unlike his brothers, did not hide from his uncle's army, nor did he attempt to fight it. Instead, he attempted to trap the army. When he came across the old manor, he enchanted it with magic, the like of which his brothers had never even dreamed. When the army came, Piranesi ran into the manor, and his uncle and the army were forever lost within it. Piranesi was not lost, however. He had bound the enchantment to four totems: a pocket watch, and amulet, a chess piece, and a coin. With them all, he used their power to open an exit from the enchantment's madness.`,
	pot: `Selecting proper tools when first endeavoring to cook is of the utmost importance. One should obtain measuring cups, a strainer, knives, tongs, spoons, and a ladle, among other things. While certainly not by any means a typical cooking utensil, I also find a good old fashioned obsidian pot to be handy to have around. Make sure to buy a good, industrial-grade one — the kind that can melt pewter, if heated properly.`,
	starting: `It seems that the house is trying to trick me: while you would think walking in one direction would lead somewhere eventually, the most counterintuitive of steps seems to lead to the rooms of most importance. Only when I walk in circles, or back and forth between two rooms, do I see some variation in the endless, mind-numbing series of halls…`,
	study: `While it has no place in a scientific study of unexplained phenomena, I must note that the local inhabitants of this area have such strange inclinations. They often walk out of rooms backwards, because, as they put it, they fear the room will "change itself" if they take their eyes off it! They have the most absurd notion of death, and insist that, in addition to others, a white flower must be placed lastly and centrally at a gravesite, in order to petition the spirits of their ancestors for guidance. Another one is their belief of secret pathways hidden around pine trees. I can find no logical explanation from which the superstition arose. None of them will even talk about the house that I wish to study, so great is their fear of it. It is quite a nuisance.`
};

const hash = s => crypto.createHash('md5')
	.update(s)
	.digest('base64')
	.replace(/[^A-Za-z0-9]/g, '')
	.substring(0, 8);

Promise.all([
		Object.assign(...Object.entries(cluetext)
			.map(([k, v]) =>
				Object.assign(...v.split('\n\n')
					.map((para, pidx) =>
						Object.assign(...para.split(/(?<=[\.!?]+)\s+/)
							.map((sent, sidx) => ({
								[`clue:${k}:${pidx}:${sidx}:${hash(sent)}`]: sent
							}))
						)
					)
				)
			)),
		fsp.readFile(path.join(process.argv[2], 'translate.json'))
		.then(raw => JSON.parse(raw.toString())),
		fsp.readFile(path.join(__dirname, '..', 'game.conf'))
		.then(raw => Object.assign({}, ...raw.toString()
			.split('\n')
			.filter(x => /^\s*(title|description)\s*=/.test(x))
			.map(x => x.replace(/^[^=]*=\s*/, ''))
			.map(x => ({
				[x]: x
			}))))
	])
	.then(objs => fsp.writeFile(path.join(__dirname, 'languages', 'en.json'),
		JSON.stringify(Object.assign(...objs), null, '    ')));
